import React, {createContext, useState} from "react";
import PokemonService from "../services/PokemonService";
const PokedexContext = createContext({
    pokemons: [],
    search: '',
    nextPage: '',
    selected: null,
    loading: false
})
const PokeListProvider = ({children}) => {
    const [pokemons, setPokemons] = useState([])
    const [search, setSearch] = useState('')
    const [nextPage, setNextPage] = useState(null)
    const [selected, setSelected] = useState(null)
    const [loading, setLoading] = useState(false)

    const resetPokemons = async () => {
        if (pokemons.length === 1) {
            setPokemons([])
            setNextPage(null)
            getPokemons()
        }
    }

    const getPokemons = async () => {
        const response = await PokemonService.getPokemons(nextPage)
        setPokemons([...pokemons, ...response.data.results])
        setNextPage(response.data.next)
    }

    const state = {
        pokemons,
        setPokemons,
        search,
        setSearch,
        nextPage,
        setNextPage,
        selected,
        setSelected,
        loading,
        setLoading,
        getPokemons,
        resetPokemons
    }

    return (
        <PokedexContext.Provider value={state}>
            {children}
        </PokedexContext.Provider>
    )
}
export {PokedexContext}

export default PokeListProvider