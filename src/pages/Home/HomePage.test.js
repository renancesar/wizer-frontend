import React from "react";
import {render} from '../../utils/TestsUtils'
import Home from './HomePage';
import {cleanup} from "@testing-library/react";

describe('Home Page Component Spec', () => {
    afterEach(cleanup)

    it('should render and compare with snapshot', () => {
        const {asFragment} = render(<Home />)
        expect(asFragment()).toMatchSnapshot()
    })

})