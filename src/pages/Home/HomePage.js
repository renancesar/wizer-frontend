import React from 'react';
import {Container, Header, Title} from './styles'
import SearchBar from "../../components/SearchBar/SearchBarComponent";
import PokeList from "../../components/PokeList/PokeListComponent";
function Home(props) {
    return (
        <Container>
            <Header></Header>
            <Title>Pokédex</Title>
            <SearchBar></SearchBar>
            <PokeList></PokeList>
        </Container>
    );
}

export default Home;