import styled from 'styled-components'
import Pokeball from '../../assets/images/pokeball.svg'
import {getColor, getFontSize} from "../../utils/ThemeUtils";

const Container = styled.div`
    display: flex;
    height: 100vh;
    background-image: url(${Pokeball});
    background-repeat: no-repeat;
    flex-direction: column;
    padding: 0 30px;
    color: ${getColor('text')};
    font-size: ${getFontSize('medium')};
`

const Header = styled.header`
  height: 60px;
`

const Title = styled.h1`
  font-size: 32px;
  color: ${getColor('title')};
`




export {Container, Header, Title}