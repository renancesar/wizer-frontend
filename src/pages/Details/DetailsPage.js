import React, {useContext, useEffect, useState} from 'react';
import {
    Container,
    DetailBody,
    DetailHeader,
    Header,
    GoBackButton,
    SectionTitle,
    DetailSection,
    PokeStatsList
} from './styles'
import PokeImage from "../../components/PokeImage/PokeImageComponent";
import PokeInfo from "../../components/PokeInfo/PokeInfoComponent";
import {PokedexContext} from "../../contexts/PokedexContext";
import {useParams, useHistory} from 'react-router-dom'
import {getPokemonType} from "../../utils/PokemonUtils";
import PokeStatistic from "../../components/PokeStatistic/PokeStatisticComponent";
import {get} from 'lodash'
import PokeAbility from "../../components/PokeAbility/PokeAbilityComponent";
import PokemonService from "../../services/PokemonService";
function Details() {
    const {id} = useParams()
    const history = useHistory()
    const Store = useContext(PokedexContext)
    const {selected, setSelected} = Store
    const [type, setType] = useState()

    useEffect(() => {
        const getAndSetSelected = async () => {
            const response = await PokemonService.getPokemonById(id)
            setSelected(response.data)
        }
        if (!selected) {
            getAndSetSelected()
        }
        setType(getPokemonType(selected))
    }, [selected, id, setSelected])

    const goToHome = () => {
        setSelected(null)
        return history.action === 'PUSH' ? history.goBack() : history.push('/')
    }

    return selected ? (
        <Container type={type}>
            <Header>
                <GoBackButton data-testid='details-go-back' onClick={goToHome}></GoBackButton>
            </Header>
            <DetailHeader>
                <PokeImage pokemon={selected} marginTop={-20}></PokeImage>
                <PokeInfo pokemon={selected} marginLeft={20}></PokeInfo>
            </DetailHeader>
            <DetailBody>
                <SectionTitle type={type}>Base Stats</SectionTitle>
                <DetailSection>
                    <PokeStatsList>
                        {get(selected, 'stats', []).map(statistic => {
                            return <PokeStatistic type={type} value={statistic.base_stat} name={statistic.stat.name} key={`${selected.id}-${statistic.stat.name}`}/>
                        })}
                    </PokeStatsList>
                </DetailSection>
                <SectionTitle type={type}>Abilities</SectionTitle>
                <DetailSection>
                    {get(selected, 'abilities', []).map(ability => <PokeAbility ability={ability} key={`${selected.id}-${ability.slot}`}></PokeAbility>)}
                </DetailSection>
            </DetailBody>
        </Container>
    ) : null
}

export default Details;