import styled from 'styled-components'
import BackButton from '../../assets/icons/back-arrow.svg'
import {getColor, getFontSize, getBackgroundByType, getColorByType} from "../../utils/ThemeUtils";
import PokeItemTexture from '../../assets/images/poke-item-texture.svg'
import PokeItemPokeball from '../../assets/images/poke-item-pokeball.svg'

const Container = styled.div`
    display: flex;
    height: 100vh;
    flex-direction: column;
    color: ${getColor('text')};
    font-size: ${getFontSize('medium')};
    background: ${getBackgroundByType};
    background-image: url(${PokeItemTexture}), url(${PokeItemPokeball});
    background-repeat: no-repeat, no-repeat;
      background-position-x: left, right;
      background-position-y: 20px, 0px;
`

const Header = styled.header`
  max-height: 60px;
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px 30px;
`

const DetailHeader = styled.div`
  padding: 0px 30px;
  flex: 1;
  display: flex;
  flex-grow: 2;
  flex-direction: row;
`

const DetailBody = styled.div`
  padding: 10px 30px 10px 30px;
  background: #FFF;
  flex: 1;
  flex-grow: 6;
  border-radius: 20px 20px 0 0;
  
`

const GoBackButton = styled.button`
  border: none;
  background: transparent;
  background-image: url(${BackButton});
  background-repeat: no-repeat;
  width: 25px;
  height: 25px;
  
`

const SectionTitle = styled.h3`
  font-weight: bold;
  font-size: ${getFontSize('medium')};
  color: ${getColorByType}
`


const DetailSection = styled.div``

const PokeStatsList = styled.div``

export {
    Container,
    Header,
    DetailHeader,
    DetailBody,
    GoBackButton,
    SectionTitle,
    DetailSection,
    PokeStatsList
}