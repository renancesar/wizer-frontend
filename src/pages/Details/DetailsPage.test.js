import React from "react";
import {render} from '../../utils/TestsUtils'
import Details from './DetailsPage';
import {cleanup, wait, fireEvent} from "@testing-library/react";
import PokemonService from "../../services/PokemonService";
import {createMemoryHistory} from 'history'
describe('Details Page Component Spec', () => {
    let mockGetPokemonById
    beforeEach(() => {
        mockGetPokemonById = jest.fn().mockResolvedValue({data: pokemon})
        PokemonService.getPokemonById = mockGetPokemonById
    })
    afterEach(() => {
        mockGetPokemonById.mockRestore()
        cleanup()
    })

    const pokemon = {
        id: 1,
        name: 'bulbasaur'
    }
    it('should render and compare with snapshot', () => {
        const {asFragment} = render(<Details />, {}, '/details/1')
        expect(asFragment()).toMatchSnapshot()
    })

    it('should not call api when has selected in context', () => {
        const store = {selected: pokemon}
        render(<Details />, {store}, '/details/1')
        wait(() => expect(mockGetPokemonById).toBeCalledTimes(0))
    })

    it('should press goBack and change route', () => {
        const history = createMemoryHistory({ initialEntries: [ '/', '/details/1' ] });
        const store = {selected: pokemon}
        const {getByTestId} = render(<Details />, {store}, null, history)
        const goBackEl = getByTestId('details-go-back')
        fireEvent.click(goBackEl)
        expect(history.location.pathname).toBe(`/`);
    })

    it('should call api when not selected in context', () => {
        render(<Details />, {}, '/details/1')
        wait(() => expect(mockGetPokemonById).toBeCalled())
    })

})