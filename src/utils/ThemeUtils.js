import {get} from 'lodash'

const getFontSize = (size) => ({theme}) => get(theme, `fontSizes.${size}`, get(theme, 'fontSizes.medium'))

const getColor = (type) => ({theme}) => get(theme, `colors.${type}`, get(theme, 'colors.text'))

const getBackgroundByType = ({theme, type}) => get(theme, `backgroundTypeColors.${type}`, get(theme, 'backgroundTypeColors.default'))

const getColorByType = ({theme, type}) => get(theme, `typeColors.${type}`, get(theme, 'typeColors.default'))
 
export {getBackgroundByType, getColor, getColorByType, getFontSize}