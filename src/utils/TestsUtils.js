import React from 'react'
import { render as rtlRender } from '@testing-library/react'
import {PokedexContext} from '../contexts/PokedexContext'
import {createMemoryHistory} from 'history'
import {Router} from "react-router-dom"

const defaultStore = {
    pokemons: [],
    setPokemons: jest.fn(),
    search: '',
    setSearch: jest.fn(),
    nextPage: null,
    setNextPage: jest.fn(),
    selected: null,
    setSelected: jest.fn(),
    loading: false,
    setLoading: jest.fn(),
    getPokemons: jest.fn(),
    resetPokemons: jest.fn()
}

function render(ui, {store = defaultStore, ...renderOptions} = {}, route = '/', history = createMemoryHistory({ initialEntries: [ route ] })) {
    function Wrapper({ children }) {
        return (
            <PokedexContext.Provider value={{...defaultStore, ...store}}>
                <Router history={history}>
                    {children}
                </Router>
            </PokedexContext.Provider>)
    }
    return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
}

// re-export everything
export * from '@testing-library/react'

// override render method
export { render }