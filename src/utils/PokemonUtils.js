import {get, first} from 'lodash'
const getPokemonType = (pokemon) => {
    const defaultType = first(get(pokemon, 'types', []))
    return get(defaultType, 'type.name')
}
export {getPokemonType}