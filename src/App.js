import React from 'react';
import './App.css';
import Theme from "./Theme";
import PokedexProvider from './contexts/PokedexContext'
import Routes from './routes'
function App() {
  return (
      <Theme>
      <PokedexProvider>
          <Routes></Routes>
      </PokedexProvider>
      </Theme>
  );
}

export default App;
