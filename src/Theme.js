import React from "react";
import { ThemeProvider } from "styled-components";

const theme = {
    colors: {
        text: "#747476",
        lightText: "#E7E7E9",
        title: "#17171B",
        sectionTitle: "#62B957",
        label: "#17171B"
    },
    fontSizes: {
        small: "12px",
        medium: "16px",
        large: "26px"
    },
    typeColors: {
        fire: "#FD7D24",
        fairy: '#ED6EC7',
        water: "#4A90DA",
        poison: "#A552CC",
        grass: "#62B957",
        electric: "#EED535",
        rock: "#BAAB82",
        dark: "#58575F",
        flying: "#748FC9",
        dragon: "#0F6AC0",
        bug: "#8CB230",
        ground: "#DD7748",
        psychic: "#EA5D60",
        fighting: "#D04164",
        ghost: '#556AAE',
        ice: '#61CEC0',
        steel: '#417D9A',
        default: "#9DA0AA"
    },
    backgroundTypeColors: {
        fire: "#FFA756",
        fairy: '#EBA8C3',
        water: "#58ABF6",
        poison: "#9F6E97",
        grass: "#8BBE8A",
        electric: "#F2CB55",
        rock: "#D4C294",
        dark: "#6F6E78",
        flying: "#83A2E3",
        dragon: "#7383B9",
        bug: "#8BD674",
        ground: "#F78551",
        psychic: "#FF6568",
        fighting: "#EB4971",
        ghost: '#8571BE',
        ice: '#91D8DF',
        steel: '#4C91B2',
        default: "#B5B9C4"
    }
};

const Theme = ({ children }) => (
    <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

export default Theme;