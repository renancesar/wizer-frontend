import styled from "styled-components";
import {getBackgroundByType, getFontSize} from "../../utils/ThemeUtils";

const PokeListContainer = styled.div`
  flex:1;
  margin-top: 30px;
`

const ButtonWrapper = styled.div`
  flex:1;
  margin-top: 30px;
  margin-bottom: 30px;
  justify-content: center;
  align-items: center;
  display:flex;
`

const ViewMoreButton = styled.button`
  color: #fff;
  background: ${getBackgroundByType};
  font-size: ${getFontSize('medium')};
  padding: 15px;
  border: none;
  border-radius: 10px
`

export {PokeListContainer, ButtonWrapper, ViewMoreButton}