import React from "react";
import {render} from '../../utils/TestsUtils'
import PokeList from './PokeListComponent';
import {cleanup, wait, fireEvent} from "@testing-library/react";
import PokemonService from "../../services/PokemonService";
describe('Poke List Component Spec', () => {
    afterEach(cleanup)
    const pokemons = [{
        name: 'bulbasaur',
        url: 'https://pokeapi.co/api/v2/pokemon/1'
    },{
        name: 'ivysaur',
        url: 'https://pokeapi.co/api/v2/pokemon/2'
    }]

    it('should render and compare with snapshot', () => {
        const {asFragment} = render(<PokeList />)
        expect(asFragment()).toMatchSnapshot()
    })

    it('should call getPokemons from provider on init component', async () => {
        const mockGetPokemons = jest.fn()
        const store = {getPokemons: mockGetPokemons, pokemons: []}
        const {asFragment} = render(<PokeList/>, {store})

        await wait(() => expect(mockGetPokemons).toBeCalled())

        expect(asFragment()).toMatchSnapshot()
        mockGetPokemons.mockRestore()
    })

    it('should not call getPokemons from provider when has more than one pokemon in list', async () => {
        const mockGetPokemons = jest.fn()
        const store = {getPokemons: mockGetPokemons, pokemons}
        const {asFragment} = render(<PokeList/>, {store})

        await wait(() => expect(mockGetPokemons).toBeCalledTimes(0))

        expect(asFragment()).toMatchSnapshot()
        mockGetPokemons.mockRestore()
    })

    it('should call getPokemons from provider when click on view more', async () => {
        const mockGetPokemons = jest.fn()
        const store = {getPokemons: mockGetPokemons, pokemons}
        const {getByText} = render(<PokeList/>, {store})

        fireEvent.click(getByText(/View More/i))

        await wait(() => expect(mockGetPokemons).toBeCalledTimes(1))
        mockGetPokemons.mockRestore()
    })

})