import React, {useContext, useEffect} from 'react';
import {PokeListContainer, ButtonWrapper, ViewMoreButton} from './styles'
import PokeItem from "../PokeItem/PokeItemComponent";
import {PokedexContext} from '../../contexts/PokedexContext'
function PokeList(props) {
    const PokedexStore = useContext(PokedexContext)
    const {pokemons, getPokemons} = PokedexStore
    useEffect(() => {
        if (!pokemons.length) getPokemons()
    }, [getPokemons, pokemons])

    return (
        <PokeListContainer>
            {pokemons.map(poke => <PokeItem key={poke.name} poke={poke} />) }
            <ButtonWrapper>
                <ViewMoreButton type='fire' onClick={getPokemons}>View More</ViewMoreButton>
            </ButtonWrapper>
        </PokeListContainer>
    );
}

export default PokeList;