import styled from "styled-components";

const PokeImageContainer = styled.div`
    flex:1;
    flex-grow: initial;
    margin-top: ${({marginTop = 0}) => `${marginTop}px`};
`

const PokeImageContent = styled.img`
  max-width: 170px;
`

export {PokeImageContainer, PokeImageContent}