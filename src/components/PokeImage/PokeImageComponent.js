import React from 'react';
import {PokeImageContainer, PokeImageContent} from './styles'
import {padStart, get} from 'lodash'

function PokeImage({pokemon, marginTop}) {
    if (!get(pokemon, 'id')) {
        return null
    }
    const id = padStart(get(pokemon, 'id'), 3, '0')
    const image = `${process.env.REACT_APP_IMAGE_BASE_URL}/${id}.png`

    return (
        <PokeImageContainer marginTop={marginTop} data-testid='poke-image'>
            <PokeImageContent src={image} alt={`${pokemon.name}`}/>
        </PokeImageContainer>
    );
}

export default PokeImage;