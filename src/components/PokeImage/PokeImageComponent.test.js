import React from "react";
import PokeImage from './PokeImageComponent';
import {cleanup, render} from "@testing-library/react";

describe('Poke Image Component Spec', () => {
    afterEach(cleanup)
    const pokemon = {
        id: 10,
        name: 'bulbasaur'
    }

    it('should render and compare with snapshot', () => {
        const {asFragment} = render(<PokeImage pokemon={pokemon} marginTop={10}/>)
        expect(asFragment()).toMatchSnapshot()
    })

    it('should render and compare with snapshot when no pass marginTop attribute', () => {
        const {asFragment, getByTestId} = render(<PokeImage pokemon={pokemon}/>)
        expect(getByTestId(/poke-image/i)).toBeDefined();
        expect(asFragment()).toMatchSnapshot()
    })

    it('should render null when pokemon no has id', () => {
        const {queryByTestId} = render(<PokeImage pokemon={{}} marginTop={10}/>)
        expect(queryByTestId(/poke-image/i)).toBeNull();
    })
})