import styled from "styled-components";
import {getColorByType} from "../../utils/ThemeUtils";
const PokeTypeContainer = styled.div`
    padding: 5px 10px;
    text-transform: capitalize;
    display: inline-block;
    color: #fff;
    background: ${getColorByType};
    margin-right: 5px;
    border-radius: 3px;
    
`
export {PokeTypeContainer}