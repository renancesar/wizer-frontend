import React from 'react';
import {PokeTypeContainer} from './styles'
import {get} from 'lodash'
function PokeTypeComponent (props) {
    const type = get(props, 'type.type')
    return (
        <PokeTypeContainer type={type.name}>{type.name}</PokeTypeContainer>
    );
}

export default PokeTypeComponent;