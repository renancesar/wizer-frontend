import styled from "styled-components";
import PokeItemTexture from '../../assets/images/poke-item-texture.svg'
import PokeItemPokeball from '../../assets/images/poke-item-pokeball.svg'
import {getBackgroundByType, getFontSize} from "../../utils/ThemeUtils";

const PokeItemContainer = styled.div`
  flex:1;
  display: flex;
  flex-direction: row;
  border-radius: 10px;
  background: ${getBackgroundByType};
  padding: 20px 0px 0px 20px; 
  margin-top: 30px;
  background-image: url(${PokeItemTexture}), url(${PokeItemPokeball});
  background-repeat: no-repeat, no-repeat;
  background-position-x: center, right;
  background-position-y: 10px, 0px;
`

const PokeId = styled.p`
  margin: 0;
  padding: 0;
  font-weight: bold;
  color: rgba(23, 23, 27, 0.6)
`

const PokeName = styled.h2`
  color: #ffffff;
  text-transform:capitalize;
  margin: 5px 0 10px 0;
  padding: 0;
  font-size: ${getFontSize('large')};
`

export {PokeItemContainer, PokeId, PokeName}