import React from "react";
import {render} from '../../utils/TestsUtils'
import PokeItem from './PokeItemComponent';
import {cleanup, wait} from "@testing-library/react";
import PokemonService from "../../services/PokemonService";
describe('Poke Item Component Spec', () => {
    afterEach(cleanup)
    const pokemon = {
        name: 'bulbasaur',
        url: 'https://pokeapi.co/api/v2/pokemon/1'
    }

    const fullPokemon = {
        id: 10,
        name: 'bulbasaur',
        types: [{
            type: {name: 'grass'}
        }]
    }

    it('should render and compare with snapshot', () => {
        const {asFragment} = render(<PokeItem poke={pokemon}/>)
        expect(asFragment()).toMatchSnapshot()
    })

    it('should call getPokemonByUrl on init component', async () => {
        const mockPokemonByUrl = jest.fn().mockResolvedValue({data: fullPokemon})
        PokemonService.getPokemonByUrl = mockPokemonByUrl
        const {asFragment} = render(<PokeItem poke={pokemon}/>)
        await wait(() => expect(mockPokemonByUrl).toBeCalled())
        expect(asFragment()).toMatchSnapshot()
        mockPokemonByUrl.mockRestore()
    })

})