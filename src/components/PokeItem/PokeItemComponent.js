import React, {useEffect, useState, memo, useContext} from 'react';
import {PokeItemContainer} from './styles'
import PokeImage from "../PokeImage/PokeImageComponent";
import PokeInfo from "../PokeInfo/PokeInfoComponent";
import {useHistory} from "react-router-dom";
import {PokedexContext} from "../../contexts/PokedexContext";
import {getPokemonType} from '../../utils/PokemonUtils'
import PokemonService from "../../services/PokemonService";
function PokeItem({poke}) {
    const Store = useContext(PokedexContext)
    const {setSelected} = Store
    const [pokemon, setPokemon] = useState({})
    let history = useHistory();

    useEffect(() => {
        async function getPokemon () {
            const response = await PokemonService.getPokemonByUrl(poke.url)
            setPokemon({...response.data})
        }
        getPokemon()
    }, [poke])

    const getType = () => getPokemonType(pokemon)

    const viewDetails = () => {
        setSelected(pokemon)
        history.push(`/details/${pokemon.id}`)
    }

    return (
        <PokeItemContainer type={getType()} onClick={viewDetails}>
            <PokeInfo pokemon={pokemon}></PokeInfo>
            <PokeImage pokemon={pokemon} marginTop={-55}/>
        </PokeItemContainer>
    );
}

export default memo(PokeItem);