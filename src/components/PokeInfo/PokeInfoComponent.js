import React from 'react';
import {PokeInfoContainer} from './styles'
import {PokeId, PokeName} from "../PokeItem/styles";
import PokeTypeComponent from "../PokeType/PokeTypeComponent";
import {get, padStart} from 'lodash'

function PokeInfo({pokemon, marginLeft}) {
    const PokeTypes = () => {
        return (
            get(pokemon, 'types', []).map(type => <PokeTypeComponent key={`${pokemon.id}-${type.slot}`} type={type} />)
        )
    }
    const getPokeId = () => padStart(pokemon.id, 3, 0)
    return (
        <PokeInfoContainer marginLeft={marginLeft}>
            <PokeId data-testid='poke-info-id'>#{getPokeId()}</PokeId>
            <PokeName>{pokemon.name}</PokeName>
            <PokeTypes />
        </PokeInfoContainer>
    );
}

export default PokeInfo;