import styled from "styled-components";

const PokeInfoContainer = styled.div`
  flex:1;
  flex-direction: column;
  margin-left: ${({marginLeft = 0}) => `${marginLeft}px`};
`

const PokeId = styled.p`
  margin: 0;
  padding: 0;
  font-weight: bold;
  color: rgba(23, 23, 27, 0.6)
`

const PokeName = styled.h2`
  color: #ffffff;
  text-transform:capitalize;
  margin: 5px 0 10px 0;
  padding: 0;
  font-size: ${({theme}) => theme.fontSizes.large};
`

export {PokeInfoContainer, PokeId, PokeName}