import React from "react";
import PokeInfo from './PokeInfoComponent';
import {cleanup, render} from "@testing-library/react";

describe('Poke Info Component Spec', () => {
    afterEach(cleanup)
    const pokemon = {
        id: 10,
        name: 'bulbasaur',
        types: [{
            type: {name: 'grass'}
        }]
    }

    it('should render and compare with snapshot', () => {
        const {asFragment} = render(<PokeInfo pokemon={pokemon} marginTop={10}/>)
        expect(asFragment()).toMatchSnapshot()
    })

    it('Should complete with zeros pokemon id', () => {
        const {getByText} = render(<PokeInfo pokemon={pokemon} marginTop={10}/>)
        const idEl = getByText(/#010/i)
        expect(idEl).toBeDefined()
    })
})