import React from 'react';
import {
    StatisticLabel,
    StatisticValue,
    StatisticValueBar,
    StatisticValueBarWrapper,
    PokeStatisticContainer
} from "./styles";

function PokeStatistic({type, value, name}) {
    return (
        <PokeStatisticContainer>
            <StatisticLabel>{name}</StatisticLabel>
            <StatisticValue>{value}</StatisticValue>
            <StatisticValueBarWrapper>
                <StatisticValueBar type={type} value={value}></StatisticValueBar>
            </StatisticValueBarWrapper>
        </PokeStatisticContainer>
    );
}

export default PokeStatistic;