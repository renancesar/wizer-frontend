import styled from "styled-components";
import {getColor, getColorByType, getFontSize} from "../../utils/ThemeUtils";

const PokeStatisticContainer = styled.div`
  display:flex;
  margin-top: 10px;
  align-items: center;
`

const StatisticLabel = styled.div`
  flex: 1;
  font-size: ${getFontSize('small')};
  color: ${getColor('title')};
  text-transform: capitalize;
`

const StatisticValue = styled.div`
  flex:1;
  flex-grow: 1;
  max-width: 30px;
  margin: 0 15px;

`

const StatisticValueBarWrapper = styled.div`
  flex-grow: 2;
 
`
const StatisticValueBar = styled.div`
  height: 4px;
  background: ${getColorByType};
  width: ${({value}) => value + '%'};
`

export {
    PokeStatisticContainer,
    StatisticLabel,
    StatisticValue,
    StatisticValueBar,
    StatisticValueBarWrapper
}