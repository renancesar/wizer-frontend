import styled from "styled-components";
import SearchIcon from '../../assets/icons/search-icon.svg'
import {getFontSize} from "../../utils/ThemeUtils";
const SearchDisclaimer = styled.p`
  margin: 0;
  font-size: ${getFontSize('medium')};
`
const SearchForm = styled.form`
  display: flex;
  position: relative;
  margin-top: 30px;
`
const SearchInput = styled.input`
    appearance: none;
    max-height: 60px;
    flex: 1;
    display: flex;
    padding: 20px 30px;
    font-size: ${getFontSize('medium')};
    width: 80%;
    border-radius: 10px;
    border: 1px solid #F2F2F2;
    background: #F2F2F2;
    padding-left: 50px;
    box-shadow: none;
`

const SearchLabel = styled.div`
    &:before {
        background-image: url(${SearchIcon});
        display: block;
        content: '';
        left: 20px;
        top: 23px;
        position: absolute;
        width: 16px;
        height: 16px;
        z-index: 99999999;
    }
`

export {SearchDisclaimer, SearchInput, SearchForm, SearchLabel}