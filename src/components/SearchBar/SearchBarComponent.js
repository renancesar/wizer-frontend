import React, {useContext, useEffect} from 'react';
import {SearchDisclaimer, SearchInput, SearchForm, SearchLabel} from './styles'
import {PokedexContext} from "../../contexts/PokedexContext";
import PokemonService from "../../services/PokemonService";
import {trim, isEmpty} from 'lodash'
import useDebounce from "../useDebounce";

function SearchBar(props) {
    const Store = useContext(PokedexContext)
    const {setPokemons, resetPokemons, search, setSearch} = Store

    const debouncedSearchTerm = useDebounce(search, 500);
    useEffect(() => {
        const searchPokemons = async () => {
            const TERM_IS_EMPTY = isEmpty(trim(debouncedSearchTerm))
            if (!TERM_IS_EMPTY) {
                const response = await PokemonService.searchPokemon(debouncedSearchTerm)
                const pokemon = response.data
                setPokemons([{...pokemon, url: `/pokemon/${pokemon.id}`}])
            }
            if (TERM_IS_EMPTY) {
                resetPokemons()
            }
        }
        searchPokemons()
    }, [debouncedSearchTerm, setPokemons, resetPokemons]
    );

    return (
        <div>
            <SearchDisclaimer>Search for Pokémon by name or using the National Pokédex number.</SearchDisclaimer>
            <SearchForm onSubmit={e => e.preventDefault()}>
                <SearchLabel></SearchLabel>
                <SearchInput
                    placeholder="What Pokémon are you looking for?"
                    value={search}
                    onChange={e => setSearch(e.target.value)}
                    data-testid='search-bar-input'
                />
            </SearchForm>
        </div>
    );
}

export default SearchBar;