import React from "react";
import {render} from '../../utils/TestsUtils'
import SearchBar from './SearchBarComponent';
import {cleanup, wait, fireEvent} from "@testing-library/react";
import PokemonService from "../../services/PokemonService";
describe('Search Bar Component Spec', () => {
    afterEach(cleanup)

    it('should render and compare with snapshot', () => {
        const {asFragment} = render(<SearchBar/>)
        expect(asFragment()).toMatchSnapshot()
    })

    it('should call resetPokemons on init component and is empty search term', async () => {
        const mockSearchPokemon = jest.fn()
        PokemonService.searchPokemon = mockSearchPokemon
        const mockResetPokemons = jest.fn()
        render(<SearchBar/>, {store: {resetPokemons: mockResetPokemons, search: ''}})

        expect(mockResetPokemons).toBeCalled()
        expect(mockSearchPokemon).toBeCalledTimes(0)

        mockSearchPokemon.mockRestore()
        mockResetPokemons.mockRestore()
    })

    it('should call searchPokemon on init component and is search term not empty', async () => {
        const mockSearchPokemon = jest.fn().mockResolvedValue({data:{}})
        PokemonService.searchPokemon = mockSearchPokemon
        const mockResetPokemons = jest.fn()
        render(<SearchBar/>, {store: {resetPokemons: mockResetPokemons, search: 'teste'}})

        expect(mockResetPokemons).toBeCalledTimes(0)
        expect(mockSearchPokemon).toBeCalled()

        mockSearchPokemon.mockRestore()
        mockResetPokemons.mockRestore()
    })

    it('should call searchPokemon on change search term', async () => {
        const mockSearchPokemon = jest.fn().mockResolvedValue({data:{}})
        PokemonService.searchPokemon = mockSearchPokemon
        const {getByTestId} = render(<SearchBar/>)

        const inputEl = getByTestId('search-bar-input')
        fireEvent.change(inputEl, { target: { value: 'ditto' } })

        wait(() => expect(mockSearchPokemon).toHaveBeenCalledWith('ditto'))

        mockSearchPokemon.mockRestore()
    })

})