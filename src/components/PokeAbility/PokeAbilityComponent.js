import React from 'react';
import {} from './styles'
import {PokeAbilityContainer} from "./styles";
import {PokeHiddenAbility} from "./styles";
import {PokeVisibleAbility} from "./styles";
function PokeAbility({ability}) {
    return (
        <PokeAbilityContainer>
            {ability.is_hidden
                ? <PokeHiddenAbility data-testid='hidden-poke-ability'>{ability.ability.name} (hidden ability)</PokeHiddenAbility>
                : <PokeVisibleAbility>{ability.slot}. {ability.ability.name}</PokeVisibleAbility>
            }
        </PokeAbilityContainer>
    );
}

export default PokeAbility;