import React from "react";
import PokeAbility from './PokeAbilityComponent';
import {cleanup, render} from "@testing-library/react";

describe('Poke Ability Component Spec', () => {
    afterEach(cleanup)
    const ability = {
        ability: {name: 'Overgrow'},
        slot: 1,
        is_hidden: false
    }

    it('should render and compare with snapshot', () => {
        const {asFragment} = render(<PokeAbility ability={ability}/>)
        expect(asFragment()).toMatchSnapshot()
    })

    it('should render hidden component when is hidden', () => {
        const {asFragment, getByTestId} = render(<PokeAbility ability={{...ability, is_hidden: true}}/>)
        expect(asFragment()).toMatchSnapshot()
        const hiddenPokeAbility = getByTestId('hidden-poke-ability')
        expect(hiddenPokeAbility).toBeInTheDocument()
    })
})