import styled from "styled-components";
import {getFontSize} from "../../utils/ThemeUtils";

const PokeAbilityContainer = styled.div`
  flex:1;
  margin-top: 5px;
`

const PokeVisibleAbility = styled.div`
  font-size: ${getFontSize('medium')};
  text-transform: capitalize;
`

const PokeHiddenAbility = styled.div`
  font-size: ${getFontSize('small')};
  text-transform: capitalize;
`

export { PokeAbilityContainer, PokeHiddenAbility, PokeVisibleAbility}