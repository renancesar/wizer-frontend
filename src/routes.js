import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import Home from './pages/Home/HomePage'
import Details from './pages/Details/DetailsPage'

export default function () {
    return (
        <Router>
            <Switch>
                <Route path="/details/:id">
                    <Details />
                </Route>
                <Route path="/">
                    <Home />
                </Route>
            </Switch>
        </Router>
    )
}