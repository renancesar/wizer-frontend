import api from "./Api";
const PokemonService = {
    getPokemons (next) {
        if (next) {
            return api.get(next)
        }
        return api.get('/pokemon')
    },

    searchPokemon (search) {
        return api.get(`/pokemon/${search}`)
    },

    getPokemonByUrl (url) {
        return api.get(url)
    },

    getPokemonById (id) {
        return api.get(`/pokemon/${id}`)
    }
}

export default PokemonService