# Wizer Frontend

[![Netlify Status](https://api.netlify.com/api/v1/badges/5b5b3aa6-73ed-427a-8fab-bdec1865f8a3/deploy-status)](https://app.netlify.com/sites/wizer-frontend/deploys)

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!
